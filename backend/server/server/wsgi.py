import os
from django.core.wsgi import get_wsgi_application
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'server.settings')
application = get_wsgi_application()

# ML registry
import inspect
from apps.ml.registry import MLRegistry
from apps.ml.predictor_precio.random_forest import RandomForestRegressor

try:
    registry = MLRegistry() # create ML registry
    # Random Forest classifier
    rf = RandomForestRegressor()
    # add to ML registry
    registry.add_algorithm(endpoint_name="predictor_precio",
                            algorithm_object=rf,
                            algorithm_name="random Forest",
                            algorithm_status="status",
                            algorithm_version="0.0.1",
                            owner="GRUPO 3: ALEXANDER CAUTIVO / OSCAR DIAZ",
                            algorithm_description="Random Forest para predecir precios de las viviendas de Boston",
                            algorithm_code=inspect.getsource(RandomForestRegressor))

except Exception as e:
    print("Exception while loading the algorithms to the registry,", str(e))